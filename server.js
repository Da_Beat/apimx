var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path')
var requestjson = require('request-json')

var bodyparser = require('body-parser')
app.use(bodyparser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

var movimientosJSON = require('./movimientosv2.json')
var urlClientes = "https://api.mlab.com/api/1/databases/dsilva/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLab = requestjson.createClient(urlClientes)

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/dsilva/collections/"
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLabRaiz;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, './index.html'))
})

app.post('/', function(request, response) {
  response.send('Hemos recibido su petición POST')
})

app.put('/', function(req, res) {
  res.send('Hemos recibido su petición PUT cambiada')
})

app.delete('/', function(req, res) {
  res.send('Hemos recibido su petición DELETE')
})

app.get('/Clientes/:idcliente', function(req, res) {
  res.send(`Aquí tiene al cliente ${req.params.idcliente}`)
})

app.get('/v1/Movimientos', (req, res) => {
  res.sendfile('./movimientosv1.json')
})

app.get('/v2/Movimientos', (req, res) => {
  res.json(movimientosJSON)
})

app.get('/v2/Movimientos/:idcliente', function(req, res) {
  res.send(movimientosJSON[req.params.idcliente-1])
})

app.get('/v2/Movimientosquery', function(req, res) {
  res.send(req.query)
})

app.post('/v2/Movimientos', (req, res) => {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send('Movimiento dado de alta')
})

app.get('/Clientes', function(req, res) {
  clienteMLab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else {
      console.log(body);
    }
  })
})

app.post('/Clientes', function(req, res) {
  clienteMLab.post('', function(err, resM, body) {
      res.send(body);
  })
})

app.get('/Usuarios', function(req, res) {
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios" + apiKey)
  clienteMLabRaiz.get('', function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else {
      console.log(body);
    }
  })
})

app.post('/Login', function(req, res) {
  // res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password

  var query = `&q={"email":"${email}", "password":"${password}"}`

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios" + apiKey + query)
  console.log(urlMLabRaiz + "Usuarios" + apiKey + query)
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err) {
      if (body.length == 1) {
        res.status(200).send("Usuario logado")
      } else {
        res.status(400).send("Usuario no encontrado")
      }
    }
  })
})
