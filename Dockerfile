# Imagen base
FROM node:latest

#Carpeta raíz del contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

# Dependencias
RUN npm install

# Puerto que exponemos
EXPOSE 3000

# Comandos para ejecutar la aplicación
CMD ["npm", "start"]

# sudo service docker restart

# sudo docker pull node
# sudo docker build -t dabeat2049/minode . # Crear imagen
# sudo docker images # Mostrar imágenes
# sudo docker run -p 2020:3000 -d dabeat2049/minode # Correr contenedor
# sudo docker ps # Mostrar contenedores
# sudo docker logs <CONTAINER ID>

# sudo docker exec -it <CONTAINER ID> /bin/bash # Ver ficheros del contenedor
# sudo docker exec -it 12ecce430702 /bin/bash
# pwd
# ls -ls
# ls -ls
# exit

# sudo docker login
# sudo docker push dabeat2049/minode

# sudo docker kill <CONTAINTER ID> # Eliminar contenedor antes de correrlo de nuevo si hay nuevos cambios
# sudo docker rmi <IMAGE ID> --force # Eliminar imagen

# git commit -m "Versión Dockerizada"
# git push origin master

# sudo docker build -t dabeat2049/minode .
# sudo docker run -p 3002:3000 -d dabeat2049/minode
# http://localhost:3002/Clientes
